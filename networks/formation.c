//
//  formation.c
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/22/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#include "formation.h"
#include <stdlib.h>
#include <string.h>

ListeFormation ajouterFormation(ListeFormation liste, Formation formation)
{
    
    /* On crée un nouvel élément */
    ListeFormation newList = (ListeFormation)malloc(sizeof(struct ListeFormation));
    
    /* On assigne la valeur au nouvel élément */
    strcpy(newList->formation.annee, formation.annee);
    strcpy(newList->formation.code, formation.code);
    strcpy(newList->formation.intitule, formation.intitule);
    
    /* On assigne l'adresse de l'élément suivant au nouvel élément */
    newList->suiv = liste;
    /* On retourne la nouvelle liste, i.e. le pointeur sur le premier élément */
    return newList;
}

void afficherFormation(Formation form) {
    char * annee1 = malloc(sizeof(char)*5);
    char * annee2 = malloc(sizeof(char)*5);
    strncpy(annee1, form.annee, 4);
    strncpy(annee2, form.annee + 4, 4);
    
    printf("Code: %s Intitule: %s Annee: %s-%s\n", form.code, form.intitule, annee1, annee2);
}

void afficherListeFormation(ListeFormation listeForm) {
    ListeFormation tmp = listeForm;
    while (tmp != NULL) {
        afficherFormation(tmp->formation);
        tmp = tmp->suiv;
    }
}
