//
//  formation.h
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/22/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#ifndef formation_h
#define formation_h

#include <stdio.h>
// Creation structure formation
typedef struct Formation
{
    char  code[20];
    char  intitule[50];
    char  annee[9];
} Formation;

typedef struct ListeFormation
{
    Formation formation;
    struct ListeFormation * suiv;
} * ListeFormation;

ListeFormation ajouterFormation(ListeFormation liste, Formation formation); // Ajoute une formation à une liste de formation
void afficherFormation(Formation form); // Affiche à l'ecran les informations concernant une formation
void afficherListeFormation(ListeFormation listeForm); // Affiche à l'ecran les formations contenues dans une liste de formations

#endif /* formation_h */
