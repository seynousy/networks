//
//  membre.c
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/21/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#include "membre.h"
#include "manip_fichiers.h"
#include "formation.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#define cheminFichierMembre "/Users/seynou/Projects/networks/networks/membres.txt"
#define cheminFichierTempMembre "/Users/seynou/Projects/networks/networks/membres_tmp.txt"

ListeMembre ajouterMembre(ListeMembre liste, Membre membre)
{
    
    /* On crée un nouvel élément */
    ListeMembre newList = (ListeMembre)malloc(sizeof(struct ListeMembre));
    /* On assigne la valeur au nouvel élément */
    strcpy(newList->membre.numero, membre.numero);
    strcpy(newList->membre.nom, membre.nom);
    strcpy(newList->membre.prenom, membre.prenom);
    strcpy(newList->membre.adresse, membre.adresse);
    newList->membre.listeFormation = membre.listeFormation;
    
    
    /* On assigne l'adresse de l'élément suivant au nouvel élément */
    newList->suiv = liste;
    /* On retourne la nouvelle liste, i.e. le pointeur sur le premier élément */
    return newList;
}

void afficherMembre(Membre membre) {
    printf("Numero: %s\nNom: %s Prenoms: %s\nAdresse: %s\nFormation(s):\n", membre.numero, membre.nom, membre.prenom, membre.adresse);
    afficherListeFormation(membre.listeFormation);
}

void afficherListeMembre(ListeMembre liste)
{
    ListeMembre tmpListe = liste;
    /* Tant que l'on n'est pas au bout de la liste */
    while(tmpListe != NULL)
    {
        /* On affiche */
        printf("-----------------\n");
        afficherMembre(tmpListe->membre);
        printf("-----------------\n");
        /* On avance d'une case */
        tmpListe = tmpListe->suiv;
    }
}

ListeMembre supprimerMembre(ListeMembre liste, char * numero)
{
    /* Liste vide, il n'y a plus rien à supprimer */
    if(liste == NULL)
        return NULL;
    
    /* Si l'élément en cours de traitement doit être supprimé */
    if(strcmp(liste->membre.numero,numero) == 0)
    {
        /* On le supprime en prenant soin de mémoriser
         l'adresse de l'élément suivant */
        ListeMembre tmp = liste->suiv;
        free(liste);
        /* L'élément ayant été supprimé, la liste commencera à l'élément suivant
         pointant sur une liste qui ne contient plus aucun élément ayant la valeur recherchée */
        return tmp;
    }
    else
    {
        /* Si l'élement en cours de traitement ne doit pas être supprimé,
         alors la liste finale commencera par cet élément et suivra une liste ne contenant
         plus d'élément ayant la valeur recherchée */
        liste->suiv = supprimerMembre(liste->suiv, numero);
        return liste;
    }
}

bool rechercherMembreBool(ListeMembre liste, char * numero)
{
    ListeMembre tmp = liste;
    /* Tant que l'on n'est pas au bout de la liste */
    while(tmp != NULL)
    {
        if(strcmp(tmp->membre.numero, numero)==0)
        {
            /* Si l'élément a la valeur recherchée, on renvoie son adresse */
            return true;
        }
        tmp = tmp->suiv;
    }
    return false;
}

Membre rechercherMembre(ListeMembre liste, char * numero)
{
    ListeMembre tmp = liste;
    Membre m;
    strcpy(m.numero, "");
    /* Tant que l'on n'est pas au bout de la liste */
    while(tmp != NULL)
    {
        if(strcmp(tmp->membre.numero, numero)==0)
        {
            /* Si l'élément a la valeur recherchée, on renvoie son adresse */
            return tmp->membre;
        }
        tmp = tmp->suiv;
    }
    return m;
}

void sauverListeMembre(ListeMembre liste) {
    remove(cheminFichierMembre);
    FILE * f = fopen(cheminFichierMembre, "w+");
    ecrire_liste(f, liste);
    fclose(f);
}


ListeMembre chargerListeMembre(){
    FILE * f = fopen(cheminFichierMembre, "r+");
    ListeMembre listeMembre = NULL;
    listeMembre = lireListeMembre(f);
    return listeMembre;
}






