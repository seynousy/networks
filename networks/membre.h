//
//  membre.h
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/21/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#ifndef membre_h
#define membre_h

#include <stdio.h>
#include <stdbool.h>
#include "./formation.h"

// Creation structure membre
typedef struct Membre
{
    char numero[20];
    char nom[20];
    char prenom[30];
    char adresse[50];
    ListeFormation listeFormation;
} Membre;

// Creation structure Liste chainée de membres
typedef struct ListeMembre
{
    Membre membre;
    struct ListeMembre * suiv;
} * ListeMembre;



ListeMembre ajouterMembre(ListeMembre liste, Membre membre); // Ajouter un membre à une liste de membres
void afficherMembre(Membre membre); // afficher à l'ecran les informations d'un membre
void afficherListeMembre(ListeMembre liste); // afficher à l'écran les informations de chaque membre d'une liste de membres
ListeMembre supprimerMembre(ListeMembre liste, char * numero); // supprimer un membre de la liste de membre
bool rechercherMembreBool(ListeMembre liste, char * numero); // recherche un membre dans une liste connaissant son numéro et retourne un booleen
Membre rechercherMembre(ListeMembre liste, char * numero); // recherche un membre dans une liste connaissant son numéro et retourne un membre en cas de succecs
void sauverListeMembre(ListeMembre liste); // enregistre la liste de membres dans un le fichier de membre
ListeMembre chargerListeMembre(); // charge la liste des membres à partir du fichier de membre





#endif /* membre_h */
