//
//  admin.h
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/21/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#ifndef admin_h
#define admin_h

#include <stdio.h>
#include <stdbool.h>

typedef struct Admin {
    char * login;
    char * mdp;
} Admin;

bool seConnecter(FILE * fp, char * login, char * mdp); // authentifie l'administrateur avec son login et mdp, retourne un booleen

#endif /* admin_h */
