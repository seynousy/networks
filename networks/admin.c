//
//  admin.c
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/21/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#include "admin.h"
#include "manip_fichiers.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

bool seConnecter(FILE * fp, char * login, char * mdp) {
    char ** content = lire_ligne(fp, 0);
    if (strcmp(login, content[0]) == 0){
        if (strcmp(mdp, content[1]) == 0) {
            printf("Connexion Réussie\n");
            return true;
        }else {
            printf("Mot de passe incorrect\n");
        }
    }else {
        printf("Login incorrect\n");
    }
    return false;
}
