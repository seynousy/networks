//
//  test.c
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/23/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#include <stdio.h>
#include "membre.h"
#include "formation.h"
#include "manip_fichiers.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "admin.h"
#define cheminFichierAdmin "/Users/seynou/Projects/networks/networks/admins.txt"
#define cheminFichierMembre "/Users/seynou/Projects/networks/networks/membres.txt"

void newMembre(Membre* membre, char * numero, char * nom, char * prenom, char * adresse, ListeFormation listeFormation){
    strcpy(membre->numero, numero);
    strcpy(membre->nom, nom);
    strcpy(membre->prenom, prenom);
    strcpy(membre->adresse, adresse);
    membre->listeFormation = listeFormation;
    /*while (listeFormationTmp != NULL) {
     membre->listeFormation = ajouterFormation(membre->listeFormation, listeFormationTmp->formation);
     listeFormationTmp = listeFormationTmp->suiv;
     }*/
}

void newFormation(Formation * form, char * code, char * intitule, char * annee){
    strcpy(form->code, code);
    strcpy(form->intitule, intitule);
    strcpy(form->annee, annee);
}

void printWelcome() {
    printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t##############################\n");
    printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t#         NETWORKS           #\n");
    printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t#   Le réseau des anciens    #\n");
    printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t##############################\n");
}

// TESTING UNIT
//int main(int argc, const char * argv[]) {
//    Formation form, form1, form2, form3, form4;
//    newFormation(&form, "dic", "inf", "20142015");
//    newFormation(&form1, "dut", "tr", "20162017");
//    newFormation(&form2, "descaf", "gestion", "20132014");
//    newFormation(&form3, "license", "elec", "20102011");
//    newFormation(&form4, "dst", "gcba", "20002001");
//    
//    ListeFormation listeFormation = NULL;
//    ListeFormation listeFormation1 = NULL;
//    ListeFormation listeFormation2 = NULL;
//    ListeFormation listeFormation3 = NULL;
//    
//    listeFormation = ajouterFormation(listeFormation, form);
//    listeFormation = ajouterFormation(listeFormation, form1);
//    listeFormation = ajouterFormation(listeFormation, form3);
//    
//    listeFormation1 = ajouterFormation(listeFormation1, form4);
//    listeFormation1 = ajouterFormation(listeFormation1, form2);
//    
//    listeFormation2 = ajouterFormation(listeFormation2, form);
//    listeFormation2 = ajouterFormation(listeFormation2, form1);
//    
//    listeFormation3 = ajouterFormation(listeFormation3, form);
//    listeFormation3 = ajouterFormation(listeFormation3, form1);
//    listeFormation3 = ajouterFormation(listeFormation3, form2);
//    listeFormation3 = ajouterFormation(listeFormation3, form3);
//    listeFormation3 = ajouterFormation(listeFormation3, form4);
//    
//    /*afficherListeFormation(listeFormation);
//     printf("-----------------\n");
//     afficherListeFormation(listeFormation1);
//     printf("-----------------\n");
//     afficherListeFormation(listeFormation2);*/
//    
//    Membre membre;
//    newMembre(&membre, "1", "ndiaye", "seynou", "maristes", listeFormation);
//    
//    Membre membre1;
//    newMembre(&membre1, "2", "sow", "modouloboly", "saintlouis", listeFormation1);
//    
//    
//    Membre membre2;
//    newMembre(&membre2, "3", "compaore", "mohamedrachid", "liberte6", listeFormation2);
//    
//    Membre membre3;
//    newMembre(&membre3, "4", "saliou", "serign saliou", "foire", listeFormation3);
//    
//    ListeMembre listeMembre = NULL;
//    listeMembre = ajouterMembre(listeMembre, membre);
//    listeMembre = ajouterMembre(listeMembre, membre1);
//    listeMembre = ajouterMembre(listeMembre, membre2);
//    listeMembre = ajouterMembre(listeMembre, membre3);
//    afficherListeMembre(listeMembre);
//    //listeMembre = supprimerMembre(listeMembre, 3);
//    printf("\n\n");
//    afficherListeMembre(listeMembre);
//    printf("Recherche en cours ... \n");
////    afficherMembre(rechercherMembre(listeMembre, 2));
////    ajouterFichierMembre(listeMembre);
//    
//    printf("FIN!\n");
//    sauverListeMembre(listeMembre);
//    
//    
//    
//    
//    ListeMembre listeMembre1;
//    listeMembre1 = chargerListeMembre();
//    printf("=================\n");
//    afficherListeMembre(listeMembre1);
//    
//    
//    return 0;
//}

int main() {
    // vars auth
    char login[20], mdp[26];
    
    // vars membres
    int choix, choixResetFormation, choixQuitter, choixModif;
    char * nom = malloc(sizeof(char)*30);
    char * prenom = malloc(sizeof(char)*30);
    char * numero = malloc(sizeof(char)*30);
    char * adresse = malloc(sizeof(char)*30);
       // formations
    char * code = malloc(sizeof(char)*30), * intitule= malloc(sizeof(char)*50), * annee= malloc(sizeof(char)*9);
    
    ListeMembre listeMembre = NULL;
    
    listeMembre = chargerListeMembre();
    afficherListeMembre(listeMembre);
    printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
    
    FILE * fichierAdmin = fopen(cheminFichierAdmin, "r");
    FILE * fichierMembre = fopen(cheminFichierMembre, "a+");
    
    // WELCOME MESSAGE
    printWelcome();
    // AUTHENTIFICATION
    bool loggedIn = false;
    do {
    connexion:
        printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t *\n");
        printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t***\n");
        printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t*\t\t  Connexion  \t\t*\n");
        printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t*\t---------------------\t*\n");
        printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tLogin: \t");
        scanf("%s", login);
        printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMot de passe: ");
        scanf("%s", mdp);
    
        loggedIn = seConnecter(fichierAdmin, login, strcat(mdp, "\n"));
        if (loggedIn == false) {
            do {
                printf("1. Réessayer\n");
                printf("2. Quitter ?\n");
                scanf("%d",&choixQuitter);
            } while (choixQuitter != 1 && choixQuitter != 2);
            if (choixQuitter == 2) {
                printf("Quitting ...");
                return 0;
            }
            else
                goto connexion;
            
        }
    } while (loggedIn == false);
    
    fclose(fichierAdmin);
    
    //OPERATIONS CRUD
    if (loggedIn == true) {
        labelMenu:
        do {
            printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t1. Ajouter un membre\n");
            printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t2. Supprimer un membre\n");
            printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t3. Rechercher un membre\n");
            printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t4. Modifier un membre\n");
            printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t5. Consulter la liste des membres\n");
            printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t6. Se Deconnecter et sauvegarder\n");
            printf("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tQue vouslez vous faire : ");
            scanf("%d", &choix);
        } while (choix > 6 || choix < 1);
    
        switch (choix) {
            case 1:
            ajout:
                printf("Veuillez entrer les informations du membre à ajouter\n");
                do {
                    printf("Numero: ");
                    scanf("%s", numero);
                    if (rechercherMembreBool(listeMembre, numero) == true)
                        printf("Membre avec ce numero existant !\n");
                } while (rechercherMembreBool(listeMembre, numero) == true);
                printf("Nom: ");
                scanf("%s", nom);
                getchar();
                
                printf("Prénom: ");
                fgets(prenom, 30, stdin);
                prenom = strtok(prenom, "\n");
                
                printf("Adresse: ");
                fgets(adresse, 50, stdin);
                adresse = strtok(adresse, "\n");
                
                Membre membre;
                
                ListeFormation listeForm = NULL;
                int i=1;
                Formation form;
                do {
                    printf("Entrez sa(ses) formation(s) suivie(s)\n");
                    printf("Formation n°%d\n", i);
                    printf("Code: ");
                    scanf("%s", code);
                    
                    printf("Intitule: ");
                    getchar();
                    fgets(intitule, 30, stdin);
                    intitule = strtok(intitule, "\n");
                    
                    printf("Année: ");
                    scanf("%s", annee);
                    
                    
                    newFormation(&form, code, intitule, annee);
                    
                    listeForm = ajouterFormation(listeForm, form);
                    
                    printf("Autre(s) formation(s) à ajouter ?\n");
                    do {
                        printf("1. Oui\n");
                        printf("2. Non\n");
                        scanf("%d", &choixResetFormation);
                    } while (choixResetFormation != 1 && choixResetFormation != 2);
                    i++;
                } while (choixResetFormation == 1);
                newMembre(&membre, numero, nom, prenom, adresse, listeForm);
                listeMembre = ajouterMembre(listeMembre, membre);
                afficherListeMembre(listeMembre);
                goto labelMenu;
                break;
            case 2:
                printf("Veulliez entrer le numero du membre à supprimer\n");
                scanf("%s", numero);
                Membre membreAtrouver;
                if (rechercherMembreBool(listeMembre, numero) == true){
                    membreAtrouver = rechercherMembre(listeMembre, numero);
                    listeMembre = supprimerMembre(listeMembre, numero);
                    afficherListeMembre(listeMembre);
                }
                else
                    printf("Membre inexistant\n");
                goto labelMenu;
                break;
            case 3:
                printf("Veuillez entrer le numero du membre à rechercher\n");
                scanf("%s", numero);
                Membre membreArechercher;
                if (rechercherMembreBool(listeMembre, numero) == true){
                    membreArechercher = rechercherMembre(listeMembre, numero);
                    afficherMembre(membreArechercher);
                }
                else
                    printf("Membre inexistant\n");
                goto labelMenu;
                break;
            case 4:
                recommencer:
                printf("Entrez le numero du membre à modifier\n");
                scanf("%s", numero);
                if (rechercherMembreBool(listeMembre, numero) == false) {
                    printf("Membre inexistant");
                    goto recommencer;
                } else {
                    do {
                        printf("1. Modifier nom ?\n");
                        printf("2. Modifier prenom ?\n");
                        printf("3. Modifier adresse ?\n");
                        printf("4. Ajouter une formation ?\n");
                        printf("5. Modifer complètement? \n");
                        scanf("%d", &choixModif);
                    } while (choixModif > 6 && choixModif < 1);
                    
                    Membre m = rechercherMembre(listeMembre, numero);
                    
                    switch (choixModif) {
                        case 1:{
                            getchar();
                            char * nom = malloc(sizeof(char) * 30);
                            printf("Entrez le nouveau nom: ");
                            scanf("%s",nom);
                            strcpy(m.nom, nom);
                            listeMembre = supprimerMembre(listeMembre, numero);
                            listeMembre = ajouterMembre(listeMembre, m);
                            break;
                        }
                            
                        case 2:{
                            char * prenom = malloc(sizeof(char) * 30);
                            getchar();
                            printf("Entrez le nouveau prenom: ");
                            fgets(prenom, 30, stdin);
                            prenom = strtok(prenom, "\n");
                            strcpy(m.prenom, prenom);
                            listeMembre = supprimerMembre(listeMembre, numero);
                            listeMembre = ajouterMembre(listeMembre, m);
                            break;
                        }
                        
                        case 3:{
                            getchar();
                            char * adresse = malloc(sizeof(char) * 50);
                            printf("Entrez le nouvel adresse: ");
                            fgets(adresse, 50, stdin);
                            adresse = strtok(adresse, "\n");
                            strcpy(m.adresse, adresse);
                            listeMembre = supprimerMembre(listeMembre, adresse);
                            listeMembre = ajouterMembre(listeMembre, m);
                            break;
                        }
                        
                        case 4:{
                            ListeFormation forms = m.listeFormation;
                            char * code = malloc(sizeof(char) * 20);
                            char * intitule = malloc(sizeof(char) * 20);
                            char * annee = malloc(sizeof(char) * 9);
                            
                            printf("Entrez les informations concernant la formation à ajouter\n");
                            printf("Code: ");
                            scanf("%s", code);
                            printf("Intitule: ");
                            getchar();
                            fgets(intitule, 30, stdin);
                            intitule = strtok(intitule, "\n");
                            printf("Année: ");
                            scanf("%s", annee);
                            
                            newFormation(&form, code, intitule, annee);
                            forms = ajouterFormation(forms, form);
                            m.listeFormation = forms;
                            listeMembre = supprimerMembre(listeMembre, numero);
                            listeMembre = ajouterMembre(listeMembre, m);
                        }
                            break;
                        
                        case 5:
                            printf("Nom: ");
                            scanf("%s", nom);
                            getchar();
                            printf("Prénom: ");
                            fgets(prenom, 30, stdin);
                            prenom = strtok(prenom, "\n");
                            printf("Adresse: ");
                            fgets(adresse, 50, stdin);
                            adresse = strtok(adresse, "\n");
                            
                            Membre membre;
                            
                            ListeFormation listeForm = NULL;
                            int i=1;
                            Formation form;
                            do {
                                printf("Entrez sa(ses) formation(s) suivie(s)\n");
                                printf("Formation n°%d\n", i);
                                printf("Code: ");
                                scanf("%s", code);
                                printf("Intitule: ");
                                getchar();
                                fgets(intitule, 30, stdin);
                                intitule = strtok(intitule, "\n");
                                printf("Année: ");
                                scanf("%s", annee);
                                
                                
                                newFormation(&form, code, intitule, annee);
                                
                                listeForm = ajouterFormation(listeForm, form);
                                
                                printf("Autre(s) formation(s) à ajouter ?\n");
                                do {
                                    printf("1. Oui\n");
                                    printf("2. Non\n");
                                    scanf("%d", &choixResetFormation);
                                } while (choixResetFormation != 1 && choixResetFormation != 2);
                                i++;
                            } while (choixResetFormation == 1);
                            newMembre(&membre, numero, nom, prenom, adresse, listeForm);
                            listeMembre = supprimerMembre(listeMembre, numero);
                            listeMembre = ajouterMembre(listeMembre, membre);
                            break;
                            
                        default:
                            printf("An unhandled error occured\n");
                            break;
                    }
                    
                    goto labelMenu;

                }
                break;
            case 5:
                afficherListeMembre(listeMembre);
                goto labelMenu;
                break;
            case 6:
                printf("Saving changes ...");
                sauverListeMembre(listeMembre);
                fclose(fichierMembre);
                printf("Disconnecting ...\n");
                break;
            default:
                printf("An unhandled error occured !\n");
                break;
        }
    } else {
        printf("Disconnected\n");
    }
    return 0;
}

