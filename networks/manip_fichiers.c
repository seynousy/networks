//
//  manip_fichiers.c
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/21/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#include "manip_fichiers.h"
#include "./membre.h"
#include "formation.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SEP ":"


int recup_nb_cols(char * contenu){
    char * contenu_cp = malloc(sizeof(char) * (strlen(contenu) + 1));
    strcpy(contenu_cp, contenu);
    int nb_cols = 0;
    char * token = strtok(contenu_cp, SEP);
    while(token !=NULL){
        token = strtok(NULL, SEP);
        nb_cols++;
    }
    free(contenu_cp);
    return nb_cols;
}

char ** lire_ligne(FILE * fp, int ligne)
{
    rewind(fp);
    int i = 0;
    char * chaine = malloc(sizeof(char) * 100);
    while (i <= ligne) {
        fgets(chaine, 100, fp);
        i++;
    }
    int nb_cols = recup_nb_cols(chaine);
    
    char ** contenu = malloc(sizeof(char *) * nb_cols);
    char *token = strtok(chaine, SEP);
    contenu[0] = malloc(sizeof(char) * (strlen(token) + 1));
    strcpy(contenu[0], token);
    for (i = 1; i < nb_cols; ++i)
    {
        token = strtok(NULL, SEP);
        contenu[i] = malloc(sizeof(char) * (strlen(token) + 1));
        strcpy(contenu[i], token);
    }
    free(chaine);
    return contenu;
}





void  ecrire_ligne(FILE * fp, char * contenu)
{
    char * chaine = NULL;
    // allouer la taille necessaire a la chaine finale
    chaine = malloc(sizeof(contenu)+1);
    strcpy(chaine, contenu);
    strcat(chaine, "\n");
    // ecrire dans le fichier
    fputs(chaine, fp);
    //free(chaine);
}



void ecrire_liste(FILE * fp, ListeMembre liste) {
    char * contenu = (char*)malloc(1500);
    ListeFormation tmpForm;
    ListeMembre tmpListe = liste;
    while (tmpListe != NULL) {
        strcpy(contenu,tmpListe->membre.numero);
        strcat(contenu,SEP);
        
        strcat(contenu, tmpListe->membre.nom);
        strcat(contenu,SEP);
        
        strcat(contenu, tmpListe->membre.prenom);
        strcat(contenu,SEP);
        
        strcat(contenu, tmpListe->membre.adresse);
        strcat(contenu,SEP);
        
        tmpForm = tmpListe->membre.listeFormation;
        
        while(tmpForm != NULL) {
            strcat(contenu, tmpForm->formation.code);
            strcat(contenu,SEP);
            strcat(contenu, tmpForm->formation.intitule);
            strcat(contenu,SEP);
            strcat(contenu, tmpForm->formation.annee);
            strcat(contenu,SEP);
            
            tmpForm = tmpForm->suiv;
        }
        ecrire_ligne(fp,contenu);
        tmpListe = tmpListe->suiv;
    }
}

Membre * parseChaineToMembre(char * contenu){
    Membre * membre = (Membre*) malloc(sizeof(Membre));
    char *token;
    ListeFormation listeForm = NULL;
    Formation form;
    
    /* get the first token */
    
    token = strtok(contenu, SEP);
    strcpy(membre->numero, token);
    
    token = strtok(NULL, SEP);
    strcpy(membre->nom, token);
    
    token = strtok(NULL, SEP);
    strcpy(membre->prenom, token);
    
    token = strtok(NULL, SEP);
    strcpy(membre->adresse, token);
    
    /* walk through other tokens */
    token = strtok(NULL, SEP);
    while( token != NULL && strcmp(token, "\n")!=0)
    {        
        strcpy(form.code, token);
        
        token = strtok(NULL, SEP);
        strcpy(form.intitule, token);
        
        token = strtok(NULL, SEP);
        strcpy(form.annee, token);
       
        listeForm = ajouterFormation(listeForm, form);
        token = strtok(NULL, SEP);
    }
    
    membre->listeFormation = listeForm;
    
    //afficherMembre(*membre);
    return membre;
}


static void purger(void)
{
    int c;
    
    while ((c = getchar()) != '\n' && c != EOF)
    {}
}

static void clean (char *chaine)
{
    char *p = strchr(chaine, '\n');
    
    if (p)
    {
        *p = 0;
    }
    
    else
    {
        purger();
    }
}


char * getLine(FILE* fp){
    char * line = (char*)malloc(1000); /* or other suitable maximum line size */
    line = fgets ( line, 1000, fp );
    return line;
}


ListeMembre lireListeMembre(FILE * fp){
    ListeMembre listeMembre = (ListeMembre)malloc(sizeof(struct ListeMembre));
    listeMembre = NULL;
    char *contenu;
    Membre * membre = NULL;
    contenu = getLine(fp);
    while ( contenu != NULL ) {
        clean(contenu);
        membre = parseChaineToMembre(contenu);
        listeMembre = ajouterMembre(listeMembre, *membre);
        contenu = getLine(fp);
    }
    return listeMembre;
}


