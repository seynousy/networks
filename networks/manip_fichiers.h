//
//  manip_fichiers.h
//  networks
//
//  Created by Papa Seynou Sy NDIAYE on 3/21/17.
//  Copyright © 2017 seynousy. All rights reserved.
//

#ifndef manip_fichiers_h
#define manip_fichiers_h

#include <stdio.h>
#include "./membre.h"

int recup_nb_cols(char * contenu); // Recupere le nombre de colonnes d'une ligne delimité par ":"
char ** lire_ligne(FILE * fp, int ligne); // Lis une ligne du fichier avec son index donné et retourne une liste de chaines de caracteres delimitee par ":"
void ecrire_ligne(FILE * fp, char * contenu); // Ecris dans un fichier une ligne avec une chaine de caracteres donnée
void ecrire_liste(FILE * fp, ListeMembre liste); // Ecris la liste des membres dans le fichier membre
ListeMembre lire_liste (FILE * fp); // Retourne la liste des membres en lisant le fichier membre
ListeMembre lireListeMembre(FILE * fp);
Membre * parseChaineToMembre(char * contenu); // Transforme une ligne du fichier membre en Membre

#endif /* manip_fichiers_h */
